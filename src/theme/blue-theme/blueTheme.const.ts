export const blue = {

  '--primary' : '#00227b',
  '--primary-variant': '#032a95',

  '--secondary': '#00227b',
  '--secondary-variant': '#fff350',//not used

  '--background': '#a8d2ea',
  '--surface': '#cee7fc',
  '--dialog': '#eaf5ff',
  '--cancel': '#9E9E9E',
  '--alt-surface': '#323232',
  '--alt-dialog': '#455a64',

  '--on-primary': '#FFFFFF',
  '--on-secondary': '#FFFFFF',
  '--on-background': '#000000',
  '--on-surface': '#000000',
  '--on-cancel': '#000000',

  '--green': '#4caf50',
  '--red': '#f44336',
  '--yellow': '#FFD54F',
  '--blue': '#3f51b5',
  '--purple': '#9c27b0',
  '--light-green': '#80ba24',//not used
  '--grey': '#BDBDBD',//not used
  '--grey-light': '#EEEEEE',//not used
  '--black': '#212121',//not used
  '--moderator': '#cee7fc'
};

export const blue_meta = {

  'translation': {
    'name': {
      'en': 'Blue Mode',
      'de': 'Blue Mode'
    },
    'description': {
      'en': 'Soft blue background for relaxed work',
      'de': 'Sanft blauer Hintergrund für entspanntes Arbeiten'
    }
  },
  'isDark': false,
  'order': 4,
  'scale_desktop': 1,
  'scale_mobile': 1,
  'previewColor': 'background'

};
